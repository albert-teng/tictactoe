# Tic-Tac-Toe

This is a simple Tic-Tac-Toe game that you can play on a Slack channel.

To initiate the game,
```
/tictactoe challenge [@username]
```

Replace `[@username]` with the username of the person you want to challenge.

For more information on the commands,
```
/tictactoe help
```