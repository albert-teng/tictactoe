<?php

class Game
{
    const GAME_STATUS_FILE = 'game_status.txt';

    /**
     * This function initialize the game
     *
     * @param  String $playerX
     * @param  String $playerO
     * @param  String $channelName
     * @return void
     */
    public function start($playerX, $playerO, $channelName)
    {
        // Create game board
        $board = array_fill(0, 3, array_fill(0, 3, ' '));

        // Latest status
        $latest = [
            'playerX'  => '@' . $playerX,
            'playerO'  => $playerO,
            'board'    => $board,
        ];
        $latest['nextTurn'] = $latest['playerX'];

        // Record the initial state of the game board
        $file = fopen($channelName . '-' . self::GAME_STATUS_FILE, 'w');
        fwrite($file, json_encode($latest));
        fclose($file);
    }

    /**
     * This function handles placing a token at the specified location
     * after checking that the move is a valid move.
     *
     * @param  String $player
     * @param  String $location
     * @param  String $channelName
     * @return Array
     */
    public function placeToken($player, $location, $channelName)
    {
        // Get the latest status from the file
        $latest = json_decode(file_get_contents($channelName . '-' . self::GAME_STATUS_FILE), true);

        // Check if this is the player's turn
        if ($player !== $latest['nextTurn']) {
            return [
                'text' => 'Please wait for your turn to place a token.'
            ];
        }

        // Check if the location specified is empty
        $locationArray = explode(',', $location);
        $row    = intval(trim($locationArray[0])) - 1; // we reduce this by one to match board array index
        $column = intval(trim($locationArray[1])) - 1; // we reduce this by one to match board array index
        if ($this->isLegalMove($latest['board'], $row, $column)) {
            // Set the token for the current player
            // and set the player for the next turn
            if ($latest['playerX'] === $player) {
                $token = 'X';
                $latest['nextTurn'] = $latest['playerO'];
            } else {
                $token = 'O';
                $latest['nextTurn'] = $latest['playerX'];
            }

            // Place token at specified location
            $latest['board'][$row][$column] = $token;

            // Check winning condition
            // Return the win message and end the game if TRUE
            if ($this->isWinningMove($latest['board'])) {
                return $this->end($player . " won the game! Congratulation!\n" . $this->prettyBoard($latest['board']) . "\nType `/tictactoe challenge [@username]` to start a new game.", $channelName);
            }

            // Check if the board is all filled up
            // if so, that means the game ends in a draw
            if ($this->isBoardFilled($latest['board'])) {
                return $this->end("Aww, this game ends in a draw.\n" . $this->prettyBoard($latest['board']) . "\nType `/tictactoe challenge [@username]` to start a new game.", $channelName);
            }

            // Update latest status file
            $file = fopen($channelName . '-' . self::GAME_STATUS_FILE, 'w');
            fwrite($file, json_encode($latest));
            fclose($file);

            // if the next turn is @slackbot, run playAsSlackbot
            // before returning the latest state of the board
            if ($latest['nextTurn'] === '@slackbot') {
                return $this->playAsSlackbot($latest['nextTurn'], $channelName);
            }

            // Return the latest state of the board
            return $this->latestState($channelName, true);
        }

        // Invalid move
        return [
            'text' => 'That is an invalid move. That spot is already filled.'
        ];
    }

    /**
     * This function helps simulate a player's turn when
     * the user is playing against @slackbot
     * @param  String $player
     * @param  String $channelName
     * @return Array
     */
    private function playAsSlackbot($player, $channelName)
    {
        // Get the latest status from the file
        $latest = json_decode(file_get_contents($channelName . '-' . self::GAME_STATUS_FILE), true);

        // Generate random location within the board
        // and check if it is a valid move
        $isValidMove = false;
        $row    = 0;
        $column = 0;
        do {
            $row    = rand(1, 3);
            $column = rand(1, 3);

            $isValidMove = $this->isLegalMove($latest['board'], $row-1, $column-1);
        } while (! $isValidMove);

        $location = $row.",".$column;

        return $this->placeToken($player, $location, $channelName);
    }

    /**
     * This function returns the latest status of the game by
     * reading the content of game status file
     *
     * @param  String   $channelName
     * @param  boolean  $isInChannel
     * @return Array
     */
    public function latestState($channelName, $isInChannel = true)
    {
        $latest = json_decode(file_get_contents($channelName . '-' . self::GAME_STATUS_FILE), true);

        return [
            'response_type' => ($isInChannel) ? 'in_channel' : 'ephemeral',
            'text' => "*X* - " . $latest['playerX'] . ", *O* - " . $latest['playerO'] . "\n\n*Next turn:* " . $latest['nextTurn'] . "\n\n" . $this->prettyBoard($latest['board']),
        ];
    }

    /**
     * This function checks if there is a game that is in progress
     *
     * @param  String $channelName
     * @return boolean
     */
    public function isInProgress($channelName)
    {
        return file_exists($channelName . '-' . self::GAME_STATUS_FILE);
    }

    /**
     * This function checks if the location is empty
     *
     * @param  Array    $board
     * @param  integer  $row
     * @param  integer  $column
     * @return boolean
     */
    private function isLegalMove($board, $row, $column)
    {
        if ($board[$row][$column] === ' ') {
            return true;
        }

        return false;
    }

    /**
     * This function checks if the last move made was the winning move.
     *
     * @param  Array   $board
     * @return boolean
     */
    private function isWinningMove($board)
    {
        // Check if current board displays a winning combo
        if (
            ($board[0][0] === $board[0][1] && $board[0][1] === $board[0][2] && $board[0][0] !== ' ') || // win on row 1
            ($board[1][0] === $board[1][1] && $board[1][1] === $board[1][2] && $board[1][0] !== ' ') || // win on row 2
            ($board[2][0] === $board[2][1] && $board[2][1] === $board[2][2] && $board[2][0] !== ' ') || // win on row 3
            ($board[0][0] === $board[1][0] && $board[1][0] === $board[2][0] && $board[0][0] !== ' ') || // win on column 1
            ($board[0][1] === $board[1][1] && $board[1][1] === $board[2][1] && $board[0][1] !== ' ') || // win on column 2
            ($board[0][2] === $board[1][2] && $board[1][2] === $board[2][2] && $board[0][2] !== ' ') || // win on column 3
            ($board[0][0] === $board[1][1] && $board[1][1] === $board[2][2] && $board[0][0] !== ' ') || // win on diagonal
            ($board[2][0] === $board[1][1] && $board[1][1] === $board[0][2] && $board[2][0] !== ' ')    // win on diagonal
        ) {
            return true;
        }

        return false;
    }

    /**
     * This function goes through the board to check if all
     * spots have been filled.
     * @param  Array    $board
     * @return boolean
     */
    private function isBoardFilled($board)
    {
        $isFilled = true;
        foreach ($board as $row) {
            foreach ($row as $column) {
                if ($column === ' ') {
                    $isFilled = false;
                }
            }
        }

        return $isFilled;
    }

    /**
     * This function runs the end game processes
     *
     * @param  String  $message
     * @return void
     */
    public function end($message, $channelName)
    {
        // Delete game status file
        if (file_exists($channelName . '-' . self::GAME_STATUS_FILE)) {
            unlink($channelName . '-' . self::GAME_STATUS_FILE);
        }

        // Display winning message
        return [
            'response_type' => 'in_channel',
            'text' => $message,
        ];
    }

    /**
     * This function formats the board array into a string for display
     *
     * @param  Array $board
     * @return String
     */
    public function prettyBoard($board)
    {
        $prettyBoard = "```\n     1     2     3  "; // column header

        foreach ($board as $row => $rowValue) {
            // Append row divider only if it's not the first column
            if ($row > 0) {
                $prettyBoard .= "\n   -----------------";
            }

            $prettyBoard .= "\n" . ($row + 1) . "  ";

            foreach ($rowValue as $column => $columnValue) {
                if (empty($columnValue)) {
                    $columnValue = " ";
                }

                // Append column divider only if it's not the first column
                if ($column > 0) {
                    $prettyBoard .= "|";
                }

                $prettyBoard .= "  " . $columnValue . "  ";
            }
        }
        $prettyBoard .= "\n```";

        return $prettyBoard;
    }
}