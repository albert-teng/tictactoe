<?php

/**
 * This function is suppose to be used as the final step that the
 * server does. Hence, it dies after echoing the JSON string.
 *
 * @param  array  $array
 * @return void
 */
function returnJSON(array $array)
{
    // Set proper header
    header("Content-type: application/json");
    http_response_code(200);

    echo json_encode($array);
    die;
}