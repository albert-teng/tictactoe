<?php

require_once 'helpers.php';
require_once 'Game.php';

// Ideally, this should be stored as an environmental variable in the server.
// But for the sake of brevity and to prevent over-engineering, I decided
// to put this here.
define('SLACK_TOKEN', 'wd1zT9BWZlbga3XYOTTfoRVB');

// Check for token validity
if (SLACK_TOKEN !== $_GET['token']) {
    returnJSON([
        'text' => "Invalid token"
    ]);
}

// Slack token is valid, proceed with parsing the command
$game = new Game;
$commandComponents = explode(' ', $_GET['text']);
switch ($commandComponents[0]) {
    case 'challenge':
        // Initiate the game by challenging someone
        if (! $game->isInProgress($_GET['channel_name'])) {
            $game->start($_GET['user_name'], $commandComponents[1], $_GET['channel_name']);
            returnJSON($game->latestState($_GET['channel_name']));
        } else {
            returnJSON([
                'response_type' => 'ephemeral',
                'text' => 'There is a game currently in progress. Please run `/tictactoe status` to view the latest status of the game.',
            ]);
        }
        break;
    case 'placeat':
        // Make a move by placing your token at specified location
        if ($game->isInProgress($_GET['channel_name'])) {
            if (strpos($commandComponents[1], ',') !== false) {
                returnJSON($game->placeToken('@'.$_GET['user_name'], $commandComponents[1], $_GET['channel_name']));
            } else {
                returnJSON([
                    'response_type' => 'ephemeral',
                    'text' => "You must use a comma `,` to separate the row value from the column value. Please refer to `/tictactoe help` for the format of an acceptable input."
                ]);
            }
        } else {
            returnJSON([
                'response_type' => 'ephemeral',
                'text' => 'There are no game in progress. You can start one by typing `/tictactoe challenge [@username]` and replacing `[@username]` with the username of the person you want to challenge.',
            ]);
        }
        break;
    case 'help':
        // Invoke the help text
        $message  = "To play this Tic-Tac-Toe game, you have the following commands at your disposal:\n\n";
        $message .= "• `/tictactoe challenge [@username]`: start a game. Replace `[@username]` with the username of the person that you want to challenge.\n";
        $message .= "• `/tictactoe placeat [row],[column]`: place your token at the specified grid position. Replace `[row]` and `[column]` with a number between 1 and 3 inclusive. Make sure that there are no space in between the comma and the `[column]` value. Otherwise, the game will treat it as an invalid move.\n";
        $message .= "• `/tictactoe help`: show this help menu.\n";
        $message .= "• `/tictactoe status`: display the current status of the board.\n";
        $message .= "• `/tictactoe ragequit`: Quit the game. This is usually frowned upon in the gaming culture.";
        returnJSON([
            'response_type' => 'ephemeral',
            'text' => $message,
        ]);
        break;
    case 'status':
        // Display the current status of the board
        if ($game->isInProgress($_GET['channel_name'])) {
            returnJSON($game->latestState($_GET['channel_name'], false));
        } else {
            returnJSON([
                'response_type' => 'ephemeral',
                'text' => 'There are no game in progress. You can start one by typing `/tictactoe challenge [@username]` and replacing `[@username]` with the username of the person you want to challenge.'
            ]);
        }
        break;
    case 'ragequit':
        // Quit the game in unsportsmanlike manner
        if ($game->isInProgress($_GET['channel_name'])) {
            returnJSON($game->end("In a fit of rage, @".$_GET['user_name']." flipped the board.\nAnd that's the end of the game, folks!", $_GET['channel_name']));
        } else {
            returnJSON([
                'response_type' => 'ephemeral',
                'text' => "Whoa! Chill out, man. There is no game is progress right now. You can't flip an empty board. What's the point in that?",
            ]);
        }
        break;
    default:
        // Catch all for invalid feedback from user
        returnJSON([
            'text' => 'Invalid command. Please refer to `/tictactoe help` for a list of valid input.'
        ]);
        break;
}
